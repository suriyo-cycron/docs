# Permissions required

In order to be able to tag and deploy a release you need to be a member of the
Release Managers groups on [gitlab.com][rm-prod] and [dev.gitlab.org][rm-dev].

[rm-prod]: https://gitlab.com/groups/gitlab-org/release/managers
[rm-dev]: https://dev.gitlab.org/groups/gitlab/release/managers

## Group memberships

Access to the release manager group is controlled through a [file in
release-tools]. As part of your [onboarding issue](./onboarding.md), you should
have edited that file to add your information, and then opened a merge request.

[file in release-tools]: https://gitlab.com/gitlab-org/release-tools/blob/master/config/release_managers.yml

## Chef Server

First make sure you have a copy of the [takeoff project](https://gitlab.com/gitlab-org/takeoff).

Now you need to request for an account on the Chef server, please [open a new issue](https://gitlab.com/gitlab-com/infrastructure/issues/new)
and an operations engineer will provide you with a private key that you can
store in `/path/to/takeoff/.chef/your_username.pem` in order to have the `knife`
tool configured. If all went well you can check if you have access to the Chef
server by running `knife status` from the takeoff directory.

Your `/path/to/takeoff/.chef/knife.rb` file should looks similar to:

```ruby
# This file goes in takeoff/.chef/knife.rb
#
# - Replace 'janedoe' with your username
# - Put your private key for chef.gitlab.com/gitlab in .chef/
#
# See http://docs.opscode.com/config_rb_knife.html for more information on knife configuration options

current_dir = File.dirname(__FILE__)
log_level                :info
log_location             STDOUT
node_name                "janedoe"
client_key               "#{current_dir}/janedoe.pem"
chef_server_url          "https://chef.gitlab.com/organizations/gitlab"
cache_type               'BasicFile'
cache_options( :path => "#{ENV['HOME']}/.chef/checksums" )
cookbook_path            ["#{current_dir}/../cookbooks"]

knife[:vault_mode] = 'client'
knife[:ssh_user] = 'janedoe'
```

---

[Return to Guides](../README.md#guides)
