# Patch Releases

Patches are released on an as-needed basis in order to fix regressions in the
current or previous [monthly releases]. Non-security patches that are outside
of our [maintenance policy] must be requested and agreed upon by the Release
Managers and the requester. This would usually happen on a [backport request issue](https://gitlab.com/gitlab-org/release/tasks/-/issues/new?issuable_template=Backporting-request) raised by the requester.

The changes included and the timing of the release is at the discretion of the
[release managers].

As a general guideline Release Managers review the number of S1 fixes as well as the overall number of fixes ready to patch. Two S1 fixes, or 10 overall fixes are both likely to trigger a patch, although the final decision rests with the Release Managers. 
 
Once the Release Managers begin preparing a patch release they may make the decision to remove any lower severity MRs that fail to merge cleanly. This is to avoid an S1 fix, or large number of other fixes from being delayed by a lower severity fix. 

## Process for Release Managers

### Before starting a patch release carefully consider whether we should patch or not

There are [good reasons not to patch](https://docs.gitlab.com/ee/policy/maintenance.html#patch-releases) bugs, especially on older versions. 

Before deciding to patch, particuarly on versions older than the current stable (for application bugs) consider:
1. How many users are affected by the bug? Consider how this impacts against the number of users who will need to upgrade to a new release.
1. Is there only one issue ready for patch or are there several? 
1. What severity do the issues have?
1. Does the issue have a workaround that can be used instead?
1. How much Release Manager time is a patch going to take? What else could we be doing with that time?

If after considering these questions we decide not to patch make sure the requester knows the reason and close any open backport requests related to the patch. Otherwise continue to the next step to start the patch release. 

### 1. Create an issue to track the patch release

In order to keep track of the various tasks that need to happen before a patch
release is considered "complete", we create an issue on the [release task tracker]
and update it as we progress.

1. Using Slack in `#releases` execute: `/chatops run release prepare <VERSION>`
    * Example: `/chatops run release prepare 11.10.3`

1. You may want to **bookmark** the issue until it's closed at the end of the
   release cycle.

It's a good idea to create a new patch issue immediately after the current
monthly release or previous patch release is completed.

### 2. Complete the patch release tasks

Use the patch issue created earlier to keep track of the process and mark off
tasks as you complete them.

## Process for Developers

Carefully read over our documentation which discusses patches that are in or out
of scope.  This can be found in our [patch maintenance
policy](https://docs.gitlab.com/ee/policy/maintenance.html#patch-releases)

#### Prepping Patch Picks

If the original MR applies cleanly to the stable branch of previous releases, we
only need to have [the appropriate label applied][pick label] on that Merge Request.

* Add the `Pick into x.xx` label to the MR for each version you want to
  backport.
* Make sure you have a Severity label applied. 
* The MR will automatically be merged once the next patch release process starts,
  and if this fails it will be skipped.

Should the MR contain conflicts, a new merge request must be created.

* Create a new MR against the corresponding stable branches, e.g. for a 12.10
  backport you'd use `12-10-stable-ee` as the target branch, and the `%12.10`
  milestone.
* Include the original changelog entry, with the `merge_request:` field still
  pointing to the original MR's IID.
* Go through the usual code review process, and after the maintainer review
  assign the MR to [release managers].
* The Danger job in the CI pipeline will fail because of the mismatched MR
  IID, but this can be ignored and won't block the merge.

---

[Return to Guides](../README.md#guides)

[monthly releases]: ../monthly.md
[release managers]: https://about.gitlab.com/community/release-managers/
[maintenance policy]: https://docs.gitlab.com/ee/policy/maintenance.html
[pick label]: https://about.gitlab.com/handbook/engineering/releases/#self-managed-releases-2
